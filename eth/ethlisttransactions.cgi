#!/bin/bash 
echo "Content-type: text/html"
echo ""

COUNT=`echo "$QUERY_STRING" | sed -n 's/^.*count=\([^&]*\).*$/\1/p' | sed "s/%20/ /g"`

TOT=`geth attach rpc:http://127.0.0.1:8545 --exec "loadScript('/home/ubuntu/ethlisttransactions.js'); listTransactions($COUNT) ;" | head -n 1`
echo $TOT
exit 0

