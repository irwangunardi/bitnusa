function listTransactions(count) {
  endBlockNumber = eth.blockNumber;
  startBlockNumber = endBlockNumber - count;
  coinbase = eth.coinbase;
    var allTransactions = []
    for (var i = startBlockNumber; i <= endBlockNumber; i++) {
      var block = eth.getBlock(i, true);
      if (block != null && block.transactions != null) {
        block.transactions.forEach( function(e) {
          if (eth.accounts.some(function(s) {return s == e.to}) && e.to != coinbase)
          {
            var transaction = {
              txid: e.hash,
              address: e.to,
              amount: web3.fromWei(e.value, "ether")
            }
            allTransactions.push(transaction)
          }
        })
      }
    }
    console.log(JSON.stringify(allTransactions));
}