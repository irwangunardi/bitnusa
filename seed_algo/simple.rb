    require 'active_support'
    require 'active_support/core_ext'
    require 'peatio_client'
    require 'uri'
    require 'json'
    require 'open-uri'


    @client = PeatioAPI::Client.new access_key: 'po5PhHICtBQ3qcJtPU2IItxKZ8D7dBOj0QhfYfM1', secret_key: 'sxPpW6Bo3agIwR7dPDxBUCFr7ecjxJd406gHfcxV', endpoint: 'https://beta.bitnusa.com', timeout: 60
    @buy = 'idr'
    @sell = 'btc'
    @market = "#{@sell}#{@buy}"
    @bitcoin_co_id_ticker = "https://vip.bitcoin.co.id/api/#{@sell}_#{@buy}/ticker"

    BUY_BASE = 0.975
    BUY_RAND = 0.04

    SELL_BASE = 1.015
    SELL_RAND = 0.04

    def cancel_orders(orders, reason)
        orders.each do |order|
            @client.post '/api/v2/order/delete', id: order['id']
            puts "Cancel order #{@market} #{order['volume']} @#{order['price']} reason: #{reason}"
        end
    end

    def get_sell_account()
        me = @client.get '/api/v2/members/me'
        me['accounts'].find{|account| account['currency'] == @sell}
    end

    def get_buy_account()
        me = @client.get '/api/v2/members/me'
        me['accounts'].find{|account| account['currency'] == @buy}
    end

    def touch_other_market()
        begin
            ticker = JSON.parse(open("https://vip.bitcoin.co.id/api/eth_idr/ticker").read.rstrip)
            ticker.deep_symbolize_keys!
            ticker = ticker[:ticker]
            
            @client.post '/api/v2/orders', market: "ethidr", side: 'buy', volume: 0.0001*Random.rand(), price: ticker[:last]
            @client.post '/api/v2/orders', market: "ethidr", side: 'sell', volume: 0.0001*Random.rand(), price: ticker[:last]

            sleep 1

            ticker = JSON.parse(open("https://vip.bitcoin.co.id/api/ltc_idr/ticker").read.rstrip)
            ticker.deep_symbolize_keys!
            ticker = ticker[:ticker]
            
            @client.post '/api/v2/orders', market: "ltcidr", side: 'buy', volume: 0.0001*Random.rand(), price: ticker[:last]
            @client.post '/api/v2/orders', market: "ltcidr", side: 'sell', volume: 0.0001*Random.rand(), price: ticker[:last]

            sleep 1

        rescue nil
        end
    end

    while true
        begin
            begin
                ticker = JSON.parse(open(@bitcoin_co_id_ticker).read.rstrip)
                ticker.deep_symbolize_keys!
                ticker = ticker[:ticker]
                puts "New #{@market} ticker at #{Time.now}: #{ticker}"
            rescue
                puts "Error when getting ticker: #{$!}"
                puts "Backtrace:\n\t#{e.backtrace.join("\n\t")}"
                sleep 5
                next
            end

            #Staging-only
            puts "Touch order #{@market} ~0.0001 @#{ticker[:last]}"
            @client.post '/api/v2/orders', market: "#{@market}", side: 'buy', volume: 0.0001*Random.rand(), price: ticker[:last]
            @client.post '/api/v2/orders', market: "#{@market}", side: 'sell', volume: 0.0001*Random.rand(), price: ticker[:last]

            orders = @client.get '/api/v2/orders', market: "#{@market}"

            sell_too_low_orders = orders.find_all {|order| order['side'] == 'sell' && order['price'].to_f < ticker[:sell].to_f*SELL_BASE}
            sell_too_high_orders = orders.find_all {|order| order['side'] == 'sell' && order['price'].to_f > ticker[:sell].to_f*(SELL_BASE+SELL_RAND)}
            buy_too_high_orders = orders.find_all {|order| order['side'] == 'buy' && order['price'].to_f > ticker[:buy].to_f*BUY_BASE}
            buy_too_low_orders = orders.find_all {|order| order['side'] == 'buy' && order['price'].to_f < ticker[:buy].to_f*(BUY_BASE-BUY_RAND)}
            expire_orders = orders.find_all {|order| order['created_at'] < Time.now - 10.minutes }

            cancel_orders(sell_too_low_orders, 'sell too low')
            cancel_orders(sell_too_high_orders, 'sell too high')
            cancel_orders(buy_too_high_orders, 'buy too high')
            cancel_orders(buy_too_low_orders, 'buy too low')
            cancel_orders(expire_orders, 'expired')

            buy_account = get_buy_account()
            while buy_account['balance'].to_f / buy_account['locked'].to_f > 0.2
                buy_price = (BUY_BASE-BUY_RAND*Random.rand())*ticker[:buy].to_f
                buy_amount = 0.05*Random.rand()*(buy_account['balance'].to_f+buy_account['locked'].to_f)
                buy_amount = [buy_account['balance'].to_f, buy_amount].min
                buy_volume = buy_amount/buy_price

                @client.post '/api/v2/orders', market: "#{@market}", side: 'buy', volume: buy_volume, price: buy_price
                puts "Buy order #{@market} #{buy_volume} @#{buy_price}"

                buy_account = get_buy_account()
            end

            sell_account = get_sell_account()
            while sell_account['balance'].to_f / sell_account['locked'].to_f > 0.2
                sell_price = (SELL_BASE+SELL_RAND*Random.rand())*ticker[:sell].to_f
                sell_volume = 0.05*Random.rand()*(sell_account['balance'].to_f+sell_account['locked'].to_f)
                sell_volume = [sell_account['balance'].to_f, sell_volume].min

                @client.post '/api/v2/orders', market: "#{@market}", side: 'sell', volume: sell_volume, price: sell_price
                puts "Sell order #{@market} #{sell_volume} @#{sell_price}"

                sell_account = get_sell_account()
            end

            touch_other_market
            
        rescue Exception => e
            puts "Error: #{$!}"
            puts "Backtrace:\n\t#{e.backtrace.join("\n\t")}"
        end
        sleep 5
    end