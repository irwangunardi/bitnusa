class CreateSelfServices < ActiveRecord::Migration
  def change
    create_table :self_services do |t|
      t.integer :currency
      t.string :txid

      t.timestamps
    end
  end
end
