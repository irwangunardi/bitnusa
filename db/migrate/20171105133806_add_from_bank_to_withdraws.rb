class AddFromBankToWithdraws < ActiveRecord::Migration
  def change
    add_column :withdraws, :from_bank, :string
  end
end
