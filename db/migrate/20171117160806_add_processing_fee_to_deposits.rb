class AddProcessingFeeToDeposits < ActiveRecord::Migration
  def change
    add_column :deposits, :processing_fee, :decimal, precision: 32, scale: 16
  end
end
