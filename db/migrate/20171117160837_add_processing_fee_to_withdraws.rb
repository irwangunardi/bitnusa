class AddProcessingFeeToWithdraws < ActiveRecord::Migration
  def change
    add_column :withdraws, :processing_fee, :decimal, precision: 32, scale: 16
  end
end
