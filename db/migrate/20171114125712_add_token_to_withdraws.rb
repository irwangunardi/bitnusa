class AddTokenToWithdraws < ActiveRecord::Migration
  def change
    add_column :withdraws, :token, :string
  end
end
