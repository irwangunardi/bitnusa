FactoryGirl.define do
  factory :fund_source do
    extra 'bitcoin'
    uid { Faker::Bitcoin.address }
    is_locked false
    currency 'btc'

    member { create(:member) }

    trait :idr do
      extra 'bc'
      uid '123412341234'
      currency 'idr'
    end

    factory :idr_fund_source, traits: [:idr]
    factory :btc_fund_source
  end
end

