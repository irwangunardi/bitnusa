class DiscourseSsoController < ApplicationController
  before_action :auth_member!, :auth_activated!, :phone_activated!

  def sso
    secret = ENV['DISCOURSE_SSO_SECRET']
    sso = SingleSignOn.parse(request.query_string, secret)
    sso.email = current_user.email

    idDocument = IdDocument.find_by(member: current_user)
    sso.name = idDocument.name if idDocument && !idDocument.name.blank? && current_user.id_document_verified?
    sso.external_id = current_user.id # unique id for each user of your application
    sso.sso_secret = secret

    redirect_to sso.to_url('http://community.'+ENV['URL_HOST']+'/session/sso_login')
  end
end