class DocumentsController < ApplicationController

  def index
    @docs ||= []

    #Hacky
    @docs << (Document.new(:key => 'price', title: t('document.price')))
    @docs << (Document.new(:key => 'api_v2', title: t('document.api_v2')))
    @docs << (Document.new(:key => 'wallet_maintainance', title: t('document.wallet_maintainance')))
    @docs << (Document.new(:key => 'txid_how', title: t('document.txid_how')))

    if current_user
      @docs += Document.all
    else
      @docs += Document.where.not(is_auth: true)
    end

  end

  def show
    @doc = Document.find_by_key(params[:id])
    raise ActiveRecord::RecordNotFound unless @doc

    if @doc.is_auth and !current_user
      redirect_to root_path, alert: t('activations.new.login_required')
    end
  end

  def api_v2
    render 'api_v2', layout: 'api_v2'
  end

  def websocket_api
    render 'websocket_api', layout: 'api_v2'
  end

  def oauth
    render 'oauth', layout: 'api_v2'
  end

  def price
    render 'price', layout: 'official_docs'
  end

  def wallet_maintainance
    render 'wallet_maintainance', layout: 'official_docs'
  end

  def terms
    render 'terms', layout: 'official_docs'
  end

  def license
    render 'license', layout: 'official_docs'
  end

  def txid_how
    render 'txid_how', layout: 'official_docs'
  end

  def deposit_mock
    render 'deposit_mock', layout: 'official_docs'
  end
end
