class WelcomeController < ApplicationController
  layout 'landing'
  include ActionView::Helpers::NumberHelper

  def index
      members_actual = Member.where(activated: true).count
      members_boosted = members_actual+100+(300-(members_actual/2))
      members_boosted = (members_actual > members_boosted) ? members_actual : members_boosted
      @event_percent = ((Math.log10(members_boosted)-Math.log10(300))/(Math.log10(50000)-Math.log10(300)))*100.round
      @event_percent = @event_percent > 100 ? 100 : @event_percent
  end
end
