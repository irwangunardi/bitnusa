require 'json'

class WebhooksController < ApplicationController
	before_action :auth_anybody!
	skip_before_filter :verify_authenticity_token

	def btc
		if params[:type] == "transaction" && params[:hash].present?
			AMQPQueue.enqueue(:deposit_coin, txid: params[:hash], channel_key: "satoshi")
			Rails.logger.info "Deposit created: #{params[:hash]}"
			render :json => { :status => "queued" }
		end
	end
	def eth
		if params[:type] == "transaction" && params[:hash].present?
			AMQPQueue.enqueue(:deposit_coin, txid: params[:hash], channel_key: "ether")
			render :json => { :status => "queued" }
		end
	end
	def ltc
		if params[:type] == "transaction" && params[:hash].present?
			AMQPQueue.enqueue(:deposit_coin, txid: params[:hash], channel_key: "litecoin")
			Rails.logger.info "Deposit created: #{params[:hash]}"
			render :json => { :status => "queued" }
		end
	end
end
