module Private
  class SelfServicesController < BaseController
    def new
      @self_service = SelfService.new
      @coins = []
      Currency.caps_enumerize.each do |c|
        @coins << c if Currency.find(c[1]).coin?
      end
    end

    def create
      if !simple_captcha_valid?
        redirect_to new_self_service_path, alert: t('.alert')
        return
      end

      @self_service = SelfService.new(self_service_params)
      @self_service.save

      if @self_service.currency_obj.coin?
        channel = DepositChannel.find_by_currency(@self_service.currency_obj.code)
        AMQPQueue.enqueue :deposit_coin, { txid: @self_service[:txid], channel_key: channel.key }
      end

      redirect_to new_self_service_path, notice: t('.notice')
    end

    private
    def self_service_params
      params.required(:self_service).permit(:currency,:txid)
    end
  end
end