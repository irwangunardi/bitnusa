@AccountSummaryUI = flight.component ->
  @attributes
    total_assets: '#total_assets'

  @updateAccount = (event, data) ->
    for currency, account of data
      amount = (new BigNumber(account.locked)).plus(new BigNumber(account.balance))

      precision = 5

      if currency == 'idr'
        precision = 0

      @$node.find("tr.#{currency} span.amount").text(formatter.round(amount, precision))
      @$node.find("tr.#{currency} span.locked").text(formatter.round(account.locked, precision))

  @updateTotalAssets = ->
    fiatCurrency = gon.fiat_currency
    symbol = gon.currencies[fiatCurrency].symbol
    sum = 0

    for currency, account of @accounts
      if currency is fiatCurrency
        sum += +account.balance
        sum += +account.locked
      else if ticker = @tickers["#{currency}#{fiatCurrency}"]
        sum += +account.balance * +ticker.last
        sum += +account.locked * +ticker.last
      else if ((ticker = @tickers["#{currency}btc"]) && (btcticker = @tickers["btc#{fiatCurrency}"]))
        sum += +account.balance * +ticker.last * + btcticker.last
        sum += +account.locked * +ticker.last * + btcticker.last


    @select('total_assets').text "≈#{symbol}#{formatter.round sum, 0}"

  @after 'initialize', ->
    @accounts = gon.accounts
    @tickers  = gon.tickers

    @on document, 'account::update', @updateAccount

    @on document, 'account::update', (event, data) =>
      @accounts = data
      @updateTotalAssets()

    @on document, 'market::tickers', (event, data) =>
      @tickers = data.raw
      @updateTotalAssets()


