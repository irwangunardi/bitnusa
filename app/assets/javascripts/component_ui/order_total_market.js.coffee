@OrderTotalMarketUI = flight.component ->
  flight.compose.mixin @, [OrderInputMixin]

  @attributes
    precision: gon.market.bid.fixed
    variables:
      input: 'total'
      output: 'volume'
      
  @onOutput = (event, order) ->
    volume = order.volume.toString().replace(/,/g, '')

    try
      total = @estimateTotal(@counterBook().slice(), BigNumber(volume))
      @trigger 'place_order::total_alert::hide'
    catch e
      total = null
      @trigger 'place_order::reset::total'
      @trigger 'place_order::total_alert::show', {label: e}
      return

    @changeOrder @value unless @validateRange(total)
    @setInputValue @value

    order.total = @value
    @trigger 'place_order::order::updated', order

  @estimateTotal = (price_levels, expected_volume) ->
    FUSE = 0.1

    throw 'market_not_deep_enough' if price_levels.length <= 0

    requiredFunds = BigNumber(0)
    start_from = BigNumber(price_levels[0][0])
    filled_at = start_from

    while true
      level = price_levels.shift()
      level_price = BigNumber(level[0])
      level_volume = BigNumber(level[1])
      filled_at = level_price

      v = BigNumber.min(level_volume, expected_volume)
      requiredFunds = requiredFunds.plus(v.times level_price)
      expected_volume = expected_volume.minus v

      break if expected_volume.equals(0) || price_levels.length == 0
      
    throw 'market_not_deep_enough' if !expected_volume.equals(0)
    throw 'volume_too_large' if (((filled_at.minus start_from).abs()).div start_from).greaterThan FUSE

    return requiredFunds
    
