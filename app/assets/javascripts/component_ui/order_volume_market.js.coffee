@OrderVolumeMarketUI = flight.component ->
  flight.compose.mixin @, [OrderInputMixin]

  @attributes
    precision: gon.market.ask.fixed
    variables:
      input: 'volume'
      output: 'total'

  @onOutput = (event, order) ->
    total = order.total.toString().replace(/,/g, '')

    try
      volume = @estimateVolume(@counterBook().slice(), BigNumber(total))
      @trigger 'place_order::volume_alert::hide'
    catch e
      volume = null
      @trigger 'place_order::reset::volume'
      @trigger 'place_order::volume_alert::show', {label: e}
      return

    @changeOrder @value unless @validateRange(volume)
    @setInputValue @value

    order.volume = @value
    @trigger 'place_order::order::updated', order

  @estimateVolume = (price_levels, expected_total) ->
    FUSE = 0.1

    throw 'market_not_deep_enough' if price_levels.length <= 0

    requiredFunds = BigNumber(0)
    start_from = BigNumber(price_levels[0][0])
    filled_at = start_from

    while true
      level = price_levels.shift()
      level_price = BigNumber(level[0])
      level_volume = BigNumber(level[1])
      level_total = level_price.times level_volume
      filled_at = level_price

      v = BigNumber.min(level_total, expected_total)
      requiredFunds = requiredFunds.plus(v.div level_price)
      expected_total = expected_total.minus v

      break if expected_total.equals(0) || price_levels.length == 0
      
    throw 'market_not_deep_enough' if !expected_total.equals(0)
    throw 'volume_too_large' if (((filled_at.minus start_from).abs()).div start_from).greaterThan FUSE

    return requiredFunds
    
