@WelcomePriceUI = flight.component ->
  @attributes
    btcidr: 'a.btcidr'
    price: 'span.price'

  @refresh = (event, data) ->
    for ticker in data.tickers
      if (ticker.market == 'btcidr')
        trend_minute = formatter.trend ticker.data.last_trend

        trend_minute = trend_minute + '-flash'

        old_text = ''
        if @select('price').length>0
          old_text = @select('price')[0].textContent

        new_text = formatter.fix_bid_market ticker.data.last, 'btcidr'

        p1 = parseFloat(ticker.data.open)
        p2 = parseFloat(ticker.data.last)
        trend = formatter.trend(p1 <= p2)

        @select('btcidr').html("BTC/IDR <span class='price'>#{new_text}</span> <span class='#{trend}'>#{formatter.price_change(p1, p2)}% </span>")
        
        if old_text != new_text
          @select('price').addClass(trend_minute)
          setTimeout(->
            @select('price').removeClass(trend_minute)
          , 800)

  @after 'initialize', ->
    @on document, 'market::tickers', @refresh

    ticker = gon.tickers["btcidr"]
    p1 = parseFloat(ticker.open)
    p2 = parseFloat(ticker.last)
    trend = formatter.trend(p1 <= p2)
    @select('btcidr').html("BTC/IDR #{formatter.fix_bid_market ticker.last, 'btcidr'} <span class='#{trend}'>#{formatter.price_change(p1, p2)}% </span>")