@ExchangesMarqueeUI = flight.component ->
  @attributes
    exchanges: 'span.exchanges'

  @refreshExchanges = (event, data) ->
    concaneted = ''

    if(data.exchanges.length > 0)
      for exchange in data.exchanges
        concaneted += exchange.exchange
        concaneted += '\xa0'
        concaneted += formatter.fixBid(exchange.price).toString()
        concaneted += '\xa0\xa0\xa0'
    else
      concaneted = 'BitNusa - Bursa jual beli digital asset generasi lanjut di Indonesia'
      
    @select('exchanges').html(concaneted)

  @after 'initialize', ->
    @on document, 'market::exchanges', @refreshExchanges
