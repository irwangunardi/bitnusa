@OrderVolumeUI = flight.component ->
  flight.compose.mixin @, [OrderInputMixin]

  @attributes
    precision: gon.market.ask.fixed
    variables:
      input: 'volume'
      known: 'price'
      output: 'total'

  @onOutput = (event, order) ->
    price = order.price.toString().replace(/,/g, '')
    total = order.total.toString().replace(/,/g, '')

    return if BigNumber(price).equals(0)
    volume = BigNumber(total).div BigNumber(price)

    @changeOrder @value unless @validateRange(volume)
    @setInputValue @value

    order.volume = @value
    @trigger 'place_order::order::updated', order
