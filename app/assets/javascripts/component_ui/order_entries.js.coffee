@OrderEntriesUI = flight.component ->
  @attributes
    marketSelector: 'a.market'
    limitSelector: 'a.limit'
    marketPanelAskSelector: '#ask_entry_market'
    marketPanelBidSelector: '#bid_entry_market'
    limitPanelAskSelector: '#ask_entry'
    limitPanelBidSelector: '#bid_entry'

  @showMarketEntries = ->
    @select('limitSelector').removeClass('active')
    @select('marketSelector').addClass('active')
    @select('marketPanelAskSelector').show()
    @select('marketPanelBidSelector').show()
    @select('limitPanelAskSelector').hide()
    @select('limitPanelBidSelector').hide()


  @showLimitEntries = ->
    @select('marketSelector').removeClass('active')
    @select('limitSelector').addClass('active')
    @select('marketPanelAskSelector').hide()
    @select('marketPanelBidSelector').hide()
    @select('limitPanelAskSelector').show()
    @select('limitPanelBidSelector').show()

  @after 'initialize', ->
    PlaceOrderUI.attachTo('#bid_entry')
    PlaceOrderUI.attachTo('#ask_entry')  
    PlaceOrderMarketUI.attachTo('#bid_entry_market')
    PlaceOrderMarketUI.attachTo('#ask_entry_market')

    @on @select('marketSelector'), 'click', @showMarketEntries
    @on @select('limitSelector'), 'click', @showLimitEntries
