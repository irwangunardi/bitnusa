@OrderTotalUI = flight.component ->
  flight.compose.mixin @, [OrderInputMixin]

  @attributes
    precision: gon.market.bid.fixed
    variables:
      input: 'total'
      known: 'price'
      output: 'volume'

  @onOutput = (event, order) ->
    price = order.price.toString().replace(/,/g, '')
    volume = order.volume.toString().replace(/,/g, '')
    total = BigNumber(price).times BigNumber(volume)

    @changeOrder @value unless @validateRange(total)
    @setInputValue @value

    order.total = @value
    @trigger 'place_order::order::updated', order
