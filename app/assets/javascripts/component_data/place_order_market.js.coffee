@PlaceOrderMarketData = flight.component ->

  @onInput = (event, data) ->
    {input: @input, output: @output} = data.variables
    @order[@input] = data.value

    return unless @order[@input]
    @trigger "place_order::output::#{@output}", @order

  @onRefresh = ->
    if @panelType() == 'bid'
      output = 'volume'
      return if !@order.total
    else
      output = 'total'
      return if !@order.volume

    @trigger "place_order::output::#{output}", @order
    

  @onReset = (event, data) ->
    {input: @input, output: @output} = data.variables
    @order[@input] = @order[@output] = null

    @trigger "place_order::reset::#{@output}"
    @trigger "place_order::order::updated", @order

  @panelType = ->
    switch @$node.attr('id')
      when 'bid_entry_market' then 'bid'
      when 'ask_entry_market' then 'ask'

  @after 'initialize', ->
    @order = {price: null, volume: null, total: null}

    @on 'place_order::input', @onInput
    @on 'place_order::refresh', @onRefresh
    @on 'place_order::reset', @onReset
