@OrderInputMixin = ->

  @attributes
    form: null
    type: null

  @reset = ->
    @text = ''
    @value = null

  @rollback = ->
    @$node.val @text

  @parseText = ->
    text  = @$node.val()
    value = BigNumber(text.toString().replace(/,/g, ''))

    switch
      when text == ''
        @reset()
        @trigger 'place_order::reset', variables: @attr.variables
        false
      when text.split('.').length>2
        @rollback()
        false
      when text[text.length-1]=='.' && @attr.precision<=0
        @rollback()
        false
      when text[text.length-1]=='.'
        false
      when value.isNaN()
        @rollback()
        false
      when value.eq(@value)
        false
      when text == @text
        false
      else
        if text.split('.').length>1 && text.split('.')[1].length > @attr.precision
          excessDecimals = text.split('.')[1].length - @attr.precision
          text  = text.substring(0, text.length-excessDecimals);
          value = BigNumber(text.toString().replace(/,/g, ''))

        startCursor = @$node.get(0).selectionStart
        endCursor = @$node.get(0).selectionEnd

        formattedVal = NumberHelpers.number_with_delimiter(value)

        selected = @text.substr(@startCursor, @endCursor - @startCursor);
        commas = selected.split(',').length - 1
        
        if formattedVal.split(',').length > @text.split(',').length
          endCursor = endCursor + 1
          startCursor = startCursor + 1
        else if formattedVal.split(',').length < @text.split(',').length
          endCursor = endCursor - 1
          startCursor = startCursor - 1

        startCursor = startCursor + commas
        endCursor = endCursor + commas

        if text.split('.').length>1 &&  /^0*$/.test(text.split('.')[1]) && text.split('.')[0] == @text.split('.')[0]
          if excessDecimals >= 0
            formattedVal = formattedVal.split('.')[0] + '.' + text.split('.')[1]
          else
            @text = text
            @value = value
            return true

        @$node.val(formattedVal)
        @$node.get(0).setSelectionRange(startCursor, endCursor);
        @text = formattedVal
        @value = value
        true

  @recordSelection = ->
    @startCursor = @$node.get(0).selectionStart
    @endCursor = @$node.get(0).selectionEnd

  @roundValueToText = (v) ->
    v.round(@attr.precision, BigNumber.ROUND_DOWN).toFixed(@attr.precision)

  @setInputValue = (v) ->
    if v?
      @text = NumberHelpers.number_with_delimiter(@roundValueToText(v))
    else
      @text = ''

    @$node.val @text

  @changeOrder = (v) ->
    @trigger 'place_order::input', variables: @attr.variables, value: v

  @process = (event) ->
    return unless @parseText()

    if @validateRange(@value)
      @changeOrder @value
    else
      @setInputValue @value

  @validateRange = (v) ->
    if @max && v.greaterThan(@max)
      @value = @max.round(@attr.precision, BigNumber.ROUND_DOWN)
      @changeOrder @max
      false
    else if v.lessThan(0)
      @value = null
      false
    else
      @value = v
      true
  
  @panelType = ->
    if @$node.attr('id').indexOf('order_bid_') != -1
      'bid'
    else
      'ask'

  @counterBook = ->
    switch @panelType()
      when 'bid' then gon.asks
      when 'ask' then gon.bids

  @onInput = (e, data) ->
    @$node.val NumberHelpers.number_with_delimiter(data[@attr.variables.input])
    @process()

  @onMax = (e, data) ->
    @max = data.max

  @onReset = (e) ->
    @$node.val ''
    @reset()
    @trigger 'place_order::input', variables: @attr.variables, value: null

  @onFocus = (e) ->
    @$node.focus()

  @after 'initialize', ->
    @orderType = @attr.type
    @text      = ''
    @value     = null

    @on @$node, 'change paste keyup', @process
    @on @$node, 'keypress', @recordSelection
    @on @attr.form, "place_order::max::#{@attr.variables.input}", @onMax
    @on @attr.form, "place_order::input::#{@attr.variables.input}", @onInput
    @on @attr.form, "place_order::output::#{@attr.variables.input}", @onOutput
    @on @attr.form, "place_order::reset::#{@attr.variables.input}", @onReset
    @on @attr.form, "place_order::focus::#{@attr.variables.input}", @onFocus
