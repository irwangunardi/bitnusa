#= require jquery
#= require bootstrap
#= require bootstrap-switch.min
#= require bignumber.min
#= require number_helpers
#= require cookies.min
#= require flight.min
#= require countdown
#= require scrollreveal.min

#= require ./lib/pusher_connection

#= require_tree ./helpers
#= require_tree ./component_data
#= require ./component_ui/welcome_price

#= require_self

$ ->
  BigNumber.config(ERRORS: false)
  GlobalData.attachTo(document, {pusher: window.pusher})
  WelcomePriceUI.attachTo('#welcome_price',{refreshTitle: false})
  WelcomePriceUI.attachTo('#welcome_price2',{refreshTitle: false})

  window.sr = ScrollReveal();
  sr.reveal('.advantage-section',{duration: 600, scale: 1, distance: '80px',easing: 'ease'});
  sr.reveal('.why-section',{duration: 600,  scale: 1, distance: '80px',easing: 'ease'});
  sr.reveal('.app-section',{duration: 600,  scale: 1, distance: '80px',easing: 'ease'});

  @mobileWeb = /Mobile|iP(hone|od|ad)|Android|BlackBerry|IEMobile|Kindle|NetFront|Silk-Accelerated|(hpw|web)OS|Fennec|Minimo|Opera M(obi|ini)|Blazer|Dolfin|Dolphin|Skyfire|Zune/i.test(navigator.userAgent)
  window.isMobile = @mobileWeb 
  $(document).ready ->
    $(window).scroll ->
      if $(this).scrollTop() > 300 && !@window.isMobile
        $('#nav_bar').fadeIn 500
      else
        $('#nav_bar').fadeOut 500
		