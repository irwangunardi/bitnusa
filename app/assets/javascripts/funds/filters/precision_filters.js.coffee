angular.module('precisionFilters', []).filter 'round_down', ->
  (number, decimals) ->
    BigNumber(number).round(decimals, BigNumber.ROUND_DOWN).toFixed(decimals)
