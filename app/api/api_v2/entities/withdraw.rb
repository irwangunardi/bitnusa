module APIv2
  module Entities
    class Withdraw < Base
      expose :id, documentation: "Unique withdraw id."
      expose :currency
      expose :sum, format_with: :decimal
      expose :amount, format_with: :decimal
      expose :fee
      expose :txid
      expose :created_at, format_with: :iso8601
      expose :done_at, format_with: :iso8601
      expose :aasm_state, as: :state
    end
  end
end
