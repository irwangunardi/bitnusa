require_relative 'validations'

module APIv2
  class Withdraws < Grape::API
    helpers ::APIv2::NamedParams

    before { authenticate! }

    desc 'Get your withdraws history.'
    params do
      use :auth
      optional :currency, type: String, values: Currency.all.map(&:code), desc: "Currency value contains  #{Currency.all.map(&:code).join(',')}"
      optional :limit, type: Integer, range: 1..100, default: 15, desc: "Set result limit."
      optional :state, type: String, values: Withdraw::STATES.map(&:to_s)
    end
    get "/withdraws" do
      withdraws = current_user.withdraws.limit(params[:limit]).recent
      withdraws = withdraws.with_currency(params[:currency]) if params[:currency]
      withdraws = withdraws.with_aasm_state(params[:state]) if params[:state].present?

      present withdraws, with: APIv2::Entities::Withdraw
    end
  end
end
