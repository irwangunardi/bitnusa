require 'net/http'
require 'uri'
require 'json'

class CoinRPC

  class JSONRPCError < RuntimeError; end
  class ConnectionRefusedError < StandardError; end

  def initialize(uri)
    @uri = URI.parse(uri)
  end

  def self.[](currency)
    c = Currency.find_by_code(currency.to_s)
    if c && c.rpc
      name = c.code.upcase
      "::CoinRPC::#{name}".constantize.new(c.rpc)
    end
  end

  def method_missing(name, *args)
    handle name, *args
  end

  def handle
    raise "Not implemented"
  end

  def http_post_request(post_body)
    http    = Net::HTTP.new(@uri.host, @uri.port)
    http.use_ssl = @uri.scheme == 'https' ? true : false
    request = Net::HTTP::Post.new(@uri.request_uri)
    request.basic_auth ENV["RPC_#{self.class.name.demodulize}_USERNAME"], ENV["RPC_#{self.class.name.demodulize}_PASSWORD"]
    request.content_type = 'application/json'
    request.body = post_body
    http.request(request).body
  rescue SystemCallError => e
    raise ConnectionRefusedError if(e.class.name.start_with?('Errno::'))
  rescue
    puts "Error on CoinRPC::#{self.class.name.demodulize}::ListTransactions: #{$!}"
    puts $!.backtrace.join("\n")
    raise
  end

  class BTC < self
    def handle(name, *args)
      post_body = { 'method' => name, 'params' => args, 'id' => 'jsonrpc' }.to_json
      resp = JSON.parse( http_post_request(post_body) )
      raise JSONRPCError, resp['error'] if resp['error']
      result = resp['result']
      result.symbolize_keys! if result.is_a? Hash
      sleep 0.1
      result
    end

    def safe_getnewaddress
      begin
        newaddress = getnewaddress("payment")
        backup_wallet
        return newaddress
      rescue SystemCallError => e
        raise ConnectionRefusedError if(e.class.name.start_with?('Errno::'))
      end
    end

    def safe_getbalance
      begin
        getbalance
      rescue SystemCallError => e
        raise ConnectionRefusedError if(e.class.name.start_with?('Errno::'))
      end
    end

    def safe_validateaddress(address_string)
      begin
        result = validateaddress(address_string)
        return result[:isvalid]
      rescue SystemCallError => e
        raise ConnectionRefusedError if(e.class.name.start_with?('Errno::'))
      end
    end

    def safe_sendtoaddress(to,amount)
      begin
         settxfee 0.00005
         return sendtoaddress(to,amount), 0
      rescue SystemCallError => e
        raise ConnectionRefusedError if(e.class.name.start_with?('Errno::'))
      end
    end

    def safe_listtransactions(account, number)
      begin
        listtransactions(account, number)
      rescue SystemCallError => e
        raise ConnectionRefusedError if(e.class.name.start_with?('Errno::'))
      end
    end

    def safe_gettransaction(txid)
      begin
        gettransaction(txid)
      rescue SystemCallError => e
        raise ConnectionRefusedError if(e.class.name.start_with?('Errno::'))
      end
    end

    def backup_wallet
        backupwallet(Currency.find_by_code(self.class.name.demodulize.underscore)[:backup_destination])
    end
  end

  class LTC < BTC
  end

  class ETH < self
    def handle(name, *args)
      post_body = {"jsonrpc" => "2.0", 'method' => name, 'params' => args, 'id' => '1' }.to_json
      resp = JSON.parse( http_post_request(post_body) )
      raise JSONRPCError, resp['error'] if resp['error']
      result = resp['result']
      result.symbolize_keys! if result.is_a? Hash
      sleep 0.1
      result
    end

    def safe_getnewaddress
      begin
        personal_newAccount("")
      rescue SystemCallError => e
        raise ConnectionRefusedError if(e.class.name.start_with?('Errno::'))
      end
    end

    def safe_getbalance #verified
      begin
        #balance = open('http://13.76.82.204/cgi-bin/ethgetbalance.cgi').read.rstrip.to_f
        coinbase = Currency.find_by_code(self.class.name.demodulize.underscore)[:coinbase]
        balance = eth_getBalance(coinbase,'latest')
        return EthMethods.from_wei(EthMethods.to_int(balance))
      rescue SystemCallError => e
        raise ConnectionRefusedError if(e.class.name.start_with?('Errno::'))
      end
    end

    def safe_validateaddress(address_string)
      address = address_string.gsub(/^0x/,'')
      return false if address == "0000000000000000000000000000000000000000"
      return false if address.length != 40
      return !(address.match(/[0-9a-fA-F]+/).nil?)
    end
 
    def safe_sendtoaddress(to, amount, from = nil)
      begin
        coinbase = Currency.find_by_code(self.class.name.demodulize.underscore)[:coinbase]
        amount = EthMethods.to_wei(amount)

        from = coinbase if from.nil?

        ##Guarantees all ether transfered to coinbase
        amount = EthMethods.to_int(eth_getBalance(from,'latest')) if to == coinbase

        plain_transaction_gas_fee = 21000
        gas_price_wei = 12000000000 # 12 Gwei
        #gas_price_hex = eth_gasPrice()
        gas_price_hex = EthMethods.to_hex(gas_price_wei)
        gas_fee = plain_transaction_gas_fee * gas_price_wei

        net_send = amount - gas_fee
      
        raise Account::BalanceError, 'Insufficient gas' if(net_send <= 0)

        personal_unlockAccount(from, "", 300) 
        return eth_sendTransaction(from: from, to: to, value: EthMethods.to_hex(net_send),  gas: EthMethods.to_hex(plain_transaction_gas_fee), gasPrice: gas_price_hex), EthMethods.from_wei(gas_fee)
      rescue SystemCallError => e
        raise ConnectionRefusedError if(e.class.name.start_with?('Errno::'))
      end
    end

    def safe_listtransactions(number) #verified
      begin
        return JSON.parse((open("#{@uri.scheme}://#{@uri.host}/cgi-bin/ethlisttransactions.cgi?count=#{number}", :http_basic_authentication => [ENV["RPC_#{self.class.name.demodulize}_USERNAME"], ENV["RPC_#{self.class.name.demodulize}_PASSWORD"]], :read_timeout => 1800).read.rstrip))
      rescue SystemCallError => e
        raise ConnectionRefusedError if(e.class.name.start_with?('Errno::'))
      end
    end

    def safe_gettransaction(txid)
      begin
        raw = eth_getTransactionByHash(txid)
        raw.symbolize_keys!

        confirmations = eth_blockNumber.to_i(16) - raw[:blockNumber].to_i(16)

        raw_1 = {
          txid: txid,
          txout: 0,
          from: raw[:from],
          to: raw[:to],
          amount:  EthMethods.from_wei(EthMethods.to_int(raw[:value])),
          confirmations: confirmations,
          timereceived: Time.now.to_datetime,
        }

        return raw_1
      rescue SystemCallError => e
        raise ConnectionRefusedError if(e.class.name.start_with?('Errno::'))
      end
    end
  end
end
