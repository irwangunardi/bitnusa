class EthMethods
    
      def self.to_wei(amount)
        return BigDecimal.new(1000000000000000000 * amount, 16).to_s.to_i rescue nil
      end
  
      def self.from_wei(amount)
        return (BigDecimal.new(amount, 16) / BigDecimal.new(1000000000000000000, 16)).to_s.to_f rescue nil
      end
  
      def self.to_int(hexstring)
        return (hexstring.gsub(/^0x/,'')[0..1] == "ff") ? (hexstring.hex - (2 ** 256)) : hexstring.hex
      end
    
      def self.to_hex(amount)
        return ('0x' + amount.to_s(16))
      end
end