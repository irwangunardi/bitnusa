module Admin
  class Ability
    include CanCan::Ability

    def initialize(user)
      return unless user.admin?

      can :read, Order
      can :read, Trade
      can :read, Proof
      can :update, Proof
      can :manage, Document
      can :manage, Member
      can :manage, Ticket
      can :manage, IdDocument
      can :manage, TwoFactor

      can :menu, Deposit

      if user.clerk_admin?
        can :read, ::Deposits::Bank
        can :read, ::Deposits::Satoshi
        can :read, ::Deposits::Ether
        can :read, ::Deposits::Litecoin
      else
        can :manage, ::Deposits::Bank
        can :manage, ::Deposits::Satoshi
        can :manage, ::Deposits::Ether
        can :manage, ::Deposits::Litecoin
      end

      can :menu, Withdraw

      if user.clerk_admin?
        can :read, ::Withdraws::Bank
        can :read, ::Withdraws::Satoshi
        can :read, ::Withdraws::Ether
        can :read, ::Withdraws::Litecoin
      else
        can :manage, ::Withdraws::Bank
        can :manage, ::Withdraws::Satoshi
        can :manage, ::Withdraws::Ether
        can :manage, ::Withdraws::Litecoin
      end

    end
  end
end
