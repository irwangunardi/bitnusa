class ActiveYamlBase < ActiveYaml::Base
  field :sort_order, default: 9999

  if Rails.env == 'test'
    set_root_path "#{Rails.root}/spec/fixtures"
  elsif Rails.env == 'production'
    set_root_path "#{Rails.root}/config"
  else
    set_root_path "#{Rails.root}/config_staging"
  end

  private

  def <=>(other)
    self.sort_order <=> other.sort_order
  end
end
