class CorpBank < ActiveYamlBase

    def self.to_hash
        return @corp_banks_hash if @corp_banks_hash
    
        @corp_banks_hash = []
        all.each {|m| @corp_banks_hash.push(m.unit_info) }
        @corp_banks_hash
    end

    def unit_info
        {name: name, account_name: account_name, account_number: account_number,opening_bank_name:opening_bank_name}
    end
end