class PaymentTransaction < ActiveRecord::Base
  extend Enumerize

  include AASM
  include AASM::Locking
  include Currencible

  STATE = [:unconfirm, :confirming, :confirmed]
  enumerize :aasm_state, in: STATE, scope: true

  validates_presence_of :txid

  has_one :deposit
  belongs_to :payment_address, foreign_key: 'address', primary_key: 'address'
  has_one :account, through: :payment_address
  has_one :member, through: :account

  after_update :sync_update

  aasm :whiny_transitions => false do
    state :unconfirm, initial: true
    state :confirming
    state :confirmed, after_commit: :deposit_accept

    event :check do |e|
      before :refresh_confirmations

      transitions :from => [:unconfirm, :confirming], :to => :confirming, :guard => :min_confirm?
      transitions :from => [:unconfirm, :confirming, :confirmed], :to => :confirmed, :guard => :max_confirm?
    end
  end

  def min_confirm?
    deposit.min_confirm?(confirmations)
  end

  def max_confirm?
    deposit.max_confirm?(confirmations)
  end

  def refresh_confirmations
    raw = CoinRPC[deposit.currency].safe_gettransaction(txid)
    self.confirmations = raw[:confirmations]
    save!
  end

  def deposit_accept
    if deposit.may_accept?
      begin
        if deposit.currency == 'eth' || deposit.currency == 'etc'
          # start forward to our centralized coinbase
          raw = CoinRPC[deposit.currency].safe_gettransaction(txid)
          begin
            txid, txfee = CoinRPC[deposit.currency].safe_sendtoaddress(Currency.find_by_code(deposit.currency)[:coinbase], deposit.amount, raw[:to])
            deposit.update_attribute(:processing_fee, txfee)
            deposit.update_attribute(:amount, deposit.amount - txfee)
          rescue Account::BalanceError => e
            Rails.logger.info("Deposit wallet #{deposit.currency} already empty: #{raw[:to]}")
          end
          # end forward to our centralized coinbase
        end
      rescue
        puts "Error on PaymentTransaction::Forwarding: #{$!}"
        puts $!.backtrace.join("\n")
      ensure
        deposit.accept! 
      end
    end
  end

  private

  def sync_update
    if self.confirmations_changed?
      ::Pusher["private-#{deposit.member.sn}"].trigger_async('deposits', { type: 'update', id: self.deposit.id, attributes: {confirmations: self.confirmations}})
    end
  end
end
