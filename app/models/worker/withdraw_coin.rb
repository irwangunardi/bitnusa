module Worker
  class WithdrawCoin

    def process(payload, metadata, delivery_info)
      payload.symbolize_keys!

      Withdraw.transaction do
        withdraw = Withdraw.lock.find payload[:id]

        return unless withdraw.processing?

        withdraw.whodunnit('Worker::WithdrawCoin') do
          withdraw.call_rpc
          withdraw.save!
        end
      end

      Withdraw.transaction do
        withdraw = Withdraw.lock.find payload[:id]

        return unless withdraw.almost_done?
        begin
          balance = CoinRPC[withdraw.currency].safe_getbalance.to_d
          raise Account::BalanceError, 'Insufficient coins' if balance < withdraw.sum
          txid, txfee = CoinRPC[withdraw.currency].safe_sendtoaddress withdraw.fund_uid, withdraw.amount.to_f
                
          withdraw.whodunnit('Worker::WithdrawCoin') do
            withdraw.update_column :txid, txid

            # withdraw.succeed! will start another transaction, cause
            # Account after_commit callbacks not to fire
            withdraw.succeed
            withdraw.save!
          end
        rescue
          puts "Error on WithdrawCoin: #{$!}"
          puts $!.backtrace.join("\n")

          withdraw.whodunnit('Worker::WithdrawCoin') do
            withdraw.fail
            withdraw.save!
          end
        end
      end
    end

  end
end
