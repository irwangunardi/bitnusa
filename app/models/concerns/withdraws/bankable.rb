module Withdraws
  module Bankable
    extend ActiveSupport::Concern

    def set_fee
      self.fee = WithdrawChannel.find_by_key(self.class.name.demodulize.underscore).try(:fee)
    end

    included do
      validates_presence_of :fund_extra

      delegate :name, to: :member, prefix: true

      alias_attribute :remark, :id
    end

  end
end
