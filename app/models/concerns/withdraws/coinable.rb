module Withdraws
  module Coinable
    extend ActiveSupport::Concern

    def set_fee
      self.fee = WithdrawChannel.find_by_key(self.class.name.demodulize.underscore).try(:fee)
    end

    def blockchain_url
      currency_obj.blockchain_url(txid)
    end

    def address_url
      currency_obj.address_url(fund_uid)
    end

    def audit!
      result = CoinRPC[currency].safe_validateaddress(fund_uid)

      if result.nil? || (result == false)
        Rails.logger.info "#{self.class.name}##{id} uses invalid address: #{fund_uid.inspect}"
        reject
        save!
      else
        super
      end
    end

    def as_json(options={})
      super(options).merge({
        blockchain_url: blockchain_url,
        address_url: address_url
      })
    end

  end
end

