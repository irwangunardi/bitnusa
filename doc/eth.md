# How to setup eth server for peatio

### What do you need?
- An instance with 4G of ram
- at least 100 G of hard disk
- Ubuntu 16.04

### adding EBS disk 

    sudo mkfs.ext4 /dev/xvdb
    mkdir /home/ubuntu/geth
    chown -R ubuntu: geth

add this line to /etc/fstab

    /dev/xvdb   /home/ubuntu/geth    ext4    defaults,nofail    0    1

then do 
    
    sudo mount /dev/xvdb /home/ubuntu/geth   
    
### Install geth

    sudo apt-get install software-properties-common
    sudo add-apt-repository -y ppa:ethereum/ethereum
    sudo apt-get update
    sudo apt-get install ethereum


### Run geth
copy the geth service file to /etc/systemd/system/eth.service
    
    sudo systemctl start eth
    sudo systemctl enable eth

### Install Nginx + fcgi

    sudo apt install nginx -y fcgiwrap
    sudo systemctl restart nginx

copy default file to /etc/nginx/sites-enabled/default

### Add nginx http authentication

    sudo apt-get update
    sudo apt-get install apache2 apache2-utils

use https://identitysafe.norton.com/password-generator/ to generate secure username & password,
take note and add it to application.yml RPC_ETH_USERNAME RPC_ETH_PASSWORD

    sudo htpasswd -c /etc/nginx/protected.htpasswd [generated_username]
    sudo systemctl restart nginx

### cgi files

copy the cgi files to /var/www/html/cgi-bin and update ethgetbalance.cgi & ethlisttransactions.cgi with your username

    sudo chown www-data:www-data -R /var/www/html/cgi-bin
    sudo chmod u+x /var/www/html/cgi-bin/*

### Install filter service
copy ethgetbalance.js to /home/ubuntu
copy ethlisttransactions.js to /home/ubuntu

    sudo chown www-data:www-data /home/ubuntu/ethgetbalance.js
    sudo chown www-data:www-data /home/ubuntu/ethlisttransactions.js

edit ethwalletnotify.rb post to eth webhook url
copy ethwalletnotify.rb file to /home/ubuntu/

    sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev nodejs ruby-all-dev ruby
    sudo gem install web3 -v 0.1.0
    sudo gem install httparty

copy ethwalletnotify.service file to /etc/systemd/system/ethwalletnotify.service

    sudo systemctl start ethwalletnotify
    sudo systemctl enable ethwalletnotify

You need to edit app/services/coin_rpc.rb enter eth IP in safe_getbalance, safe_listtransactions
You only need to put your the IP and /rpc in currencies.yml

### Coinbase account setup

geth (--rinkeby) attach rpc:http://127.0.0.1:8545
personal.newAccount('')
miner.setEtherbase(eth.accounts[0])
Edit coinbase on currencies.yml


