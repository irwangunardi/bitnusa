# How to run Peatio and bitcoind on  Separate  Servers on AWS 

### what do you need?
- micro instance (for bitcoind don't create now)
- medium instance (for peatio) 
- EBS disk with 150 GB (To download the full node)
You should follow this steps [here](https://github.com/muhammednagy/peatio/blob/master/doc/deploy-production-server.md) to deploy Peatio ignore the bitcoind part

### create Bitcoind Server
1. Login to AWS Dashboard
2. head to EC2 
3. make a new micro instance and choose Ubuntu 16.04
4. add new 150 GB disk and mount it on /dev/xvdb
5. open SSH port to the world or your IP only if you have static IP  
6. allow port 8333 for the security group (default launch-wizard-1 )

### adding EBS disk 
    sudo mkfs.ext4 /dev/xvdb
    mkdir /home/ubuntu/bitcoind
    chown -R ubuntu: bitcoind

add this line to /etc/fstab

    /dev/xvdb   /home/ubuntu/bitcoind    ext4    defaults,nofail    0    1

then do 
    
    sudo mount /dev/xvdb /home/ubuntu/bitcoind    
### Installing bitcoind Server

SSH into the Server and execute these commands

    sudo add-apt-repository ppa:bitcoin/bitcoin
    sudo apt-get update
    sudo apt-get install bitcoind

### Configuring bitcoind

    touch ~/bitcoind/bitcoin.conf
    nano ~/bitcoind/bitcoin.conf

Insert the following lines into the bitcoin.conf, and replce with your username and password.

    server=1
    daemon=1
    
    #rescan=1
    #printtoconsole=1

    # If run on the test network instead of the real bitcoin network
    #testnet=1

    # You must set rpcuser and rpcpassword to secure the JSON-RPC api
    # Please make rpcpassword to something secure, `5gKAgrJv8CQr2CGUhjVbBFLSj29HnE6YGXvfykHJzS3k` for example.
    # Listen for JSON-RPC connections on <port> (default: 8332 or testnet: 18332)
    # use https://identitysafe.norton.com/password-generator/ to randomize username & password

    rpcuser=QuBEtrUfrejA7rAtuvA7eSudRA538Pru
    rpcpassword=sugecr7batra46dU7reYavadrU7us2ec
    rpcport=18332
    rpcallowip=127.0.0.1      

    # Set tx fee
    txconfirmtarget=2

    # Notify when receiving coins
    walletnotify=/usr/bin/curl -d '{"type":"transaction", "hash":"%s"}' -H "Content-Type: application/json" -X POST https://bitnusa.com/webhooks/btc


### Run bitcoind automatically 

    sudo touch /etc/systemd/system/bitcoind.service
    sudo nano /etc/systemd/system/bitcoind.service

insert these lines into the file

[Unit]
Description=Bitcoin's distributed currency daemon
After=network.target

[Service]
User=ubuntu
Group=ubuntu

Type=forking
PIDFile=/home/ubuntu/bitcoind/bitcoind.pid
ExecStart=/usr/bin/bitcoind -daemon -pid=/home/ubuntu/bitcoind/bitcoind.pid -conf=/home/ubuntu/bitcoind/bitcoin.conf -datadir=/home/ubuntu/bitcoind/

Restart=always
PrivateTmp=true
TimeoutStopSec=60s
TimeoutStartSec=2s
StartLimitInterval=120s
StartLimitBurst=5

[Install]
WantedBy=multi-user.target


save the file and run

    sudo systemctl start bitcoind
    sudo systemctl enable bitcoind # to enable automatic start when you reboot the server

# nginx (optional)
install nginx

    sudo apt-get update
    sudo apt-get install nginx

add nginx http authentication

    sudo apt-get update
    sudo apt-get install apache2 apache2-utils

use https://identitysafe.norton.com/password-generator/ to generate secure username & password,
take note and add it to application.yml RPC_BTC_USERNAME RPC_BTC_PASSWORD

    sudo htpasswd -c /etc/nginx/protected.htpasswd du36chuchABracRAwruba6p4refra6eb

create default file in /etc/nginx/sites-enabled/default
for authentication you can use https://www.base64encode.org/ using configured bitcoind username & password

    server {
        listen 443 ssl http2 default;
        ssl                  on;
        ssl_certificate      /etc/nginx/ssl/bitnusa_com/ssl-bundle.crt;
        ssl_certificate_key  /etc/nginx/ssl/bitnusa_com/bitnusa.key;
        
        ssl_ciphers ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS;
        ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
        ssl_prefer_server_ciphers on;
        ssl_session_cache shared:SSL:60m;
        ssl_session_timeout  60m;
        
        # ssl_stapling on; # Requires nginx >= 1.3.7
        # ssl_stapling_verify on; # Requires nginx => 1.3.7
        # resolver 8.8.4.4 8.8.8.8 valid=300s;
        # resolver_timeout 5s;

        server_name _;

        location /rpc { 
            auth_basic "Restricted access to this site";
            auth_basic_user_file /etc/nginx/protected.htpasswd;
            proxy_pass http://localhost:18332/;
            proxy_set_header Authorization "Basic base64(user:pass)";
        }
    }

restart nginx

    sudo systemctl restart nginx

# done 
You only need to put your the IP and /rpc in currencies.yml
