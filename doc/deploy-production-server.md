Deploy production server on Ubuntu 16.04
-------------------------------------

### Overview

1. Setup deploy user
2. Install [Ruby](https://www.ruby-lang.org/en/)
3. Install [MySQL](http://www.mysql.com/)
4. Install [Redis](http://redis.io/)
5. Install [RabbitMQ](https://www.rabbitmq.com/)
6. Install [Bitcoind](https://en.bitcoin.it/wiki/Bitcoind)
7. Install [Nginx with Passenger](https://www.phusionpassenger.com/)
8. Install JavaScript Runtime
9. Install ImageMagick
10. Configure Peatio

### 1. Setup deploy user

Create (if it doesn’t exist) deploy user, and assign it to the sudo group:

    sudo adduser deploy
    sudo usermod -a -G sudo deploy

Re-login as deploy user

### 2. Install Ruby

Make sure your system is up-to-date.

    sudo apt-get update
    sudo apt-get upgrade

Installing [rbenv](https://github.com/sstephenson/rbenv) using a Installer

    sudo apt-get install git-core curl zlib1g-dev build-essential \
                         libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 \
                         libxml2-dev libxslt1-dev libcurl4-openssl-dev \
                         python-software-properties libffi-dev

    cd
    git clone git://github.com/sstephenson/rbenv.git .rbenv
    echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
    echo 'eval "$(rbenv init -)"' >> ~/.bashrc
    exec $SHELL

    git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
    echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
    exec $SHELL

Install Ruby through rbenv:

    rbenv install 2.2.1
    rbenv global 2.2.1

Install bundler

    echo "gem: --no-ri --no-rdoc" > ~/.gemrc
    gem install bundler
    rbenv rehash

### 3. Install MySQL

    sudo apt-get install mysql-server  mysql-client  libmysqlclient-dev

### 4. Install Redis

    sudo apt install -y redis-server 

### 5. Install RabbitMQ

Please follow instructions here: https://www.rabbitmq.com/install-debian.html

    echo 'deb http://www.rabbitmq.com/debian/ testing main' | sudo tee /etc/apt/sources.list.d/rabbitmq.list
    wget -O- https://www.rabbitmq.com/rabbitmq-release-signing-key.asc | sudo apt-key add -
    sudo apt-get update
    sudo apt-get install rabbitmq-server

    sudo rabbitmq-plugins enable rabbitmq_management
    sudo service rabbitmq-server restart
    wget http://localhost:15672/cli/rabbitmqadmin
    chmod +x rabbitmqadmin
    sudo mv rabbitmqadmin /usr/local/sbin


### 6. Installing Nginx & Passenger

Install Phusion's PGP key to verify packages

    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7

Add HTTPS support to APT

    sudo apt-get install apt-transport-https ca-certificates

Add the passenger repository. Note that this only works for Ubuntu 16.04. For other versions of Ubuntu, you have to add the appropriate 
repository according to Section 2.3.1 of this [link](https://www.phusionpassenger.com/documentation/Users%20guide%20Nginx.html).

    sudo sh -c 'echo deb https://oss-binaries.phusionpassenger.com/apt/passenger xenial main > /etc/apt/sources.list.d/passenger.list'
    sudo apt-get update

Install nginx and passenger

    sudo apt-get install nginx-extras passenger

Next, we need to update the Nginx configuration to point Passenger to the version of Ruby that we're using. You'll want to open up /etc/nginx/nginx.conf in your favorite editor,

    sudo nano /etc/nginx/passenger.conf

find the following lines, and uncomment them:

    passenger_root /usr/lib/ruby/vendor_ruby/phusion_passenger/locations.ini;
    passenger_ruby /usr/bin/ruby;

update the second line to read:

    passenger_ruby /home/deploy/.rbenv/shims/ruby;

we will alsp need to enable passenger in nginx config file
  
    sudo nano /etc/nginx/nginx.conf 

and uncomment

    include  /etc/nginx/passenger.conf;.

### 7. Install JavaScript Runtime

A JavaScript Runtime is needed for Asset Pipeline to work. Any runtime will do but Node.js is recommended.

    curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
    sudo apt-get install nodejs


### 8. Install ImageMagick

    sudo apt-get -y install imagemagick gsfonts


### 9. Setup production environment variable

    echo "export RAILS_ENV=production" >> ~/.bashrc
    source ~/.bashrc

### IF NOT USING MINA

    ##### Clone the Source

        mkdir -p ~/bitnusa
        git clone git@gitlab.com:irwangunardi/bitnusa.git ~/bitnusa/current
        cd bitnusa/current

        ＃ Install dependency gems
        bundle install --without development test --path vendor/bundle

    ##### Configure Peatio

    **Prepare configure files**

        bin/init_config

    **Setup Pusher**

    * Peatio depends on [Pusher](http://pusher.com). A development key/secret pair for development/test is provided in `config/application.yml` (uncomment to use). PLEASE USE IT IN DEVELOPMENT/TEST ENVIRONMENT ONLY!

    More details to visit [pusher official website](http://pusher.com)

        # uncomment Pusher related settings
        vim config/application.yml

    **Setup bitcoind rpc endpoint**

        # replace username:password and port with the one you set in
        # username and password should only contain letters and numbers, do not use email as username
        # bitcoin.conf in previous step
        vim config/currencies.yml

    **Config database settings**

        vim config/database.yml

        # Initialize the database and load the seed data
        bundle exec rake db:setup

    **Precompile assets**

        bundle exec rake assets:precompile

    **Run Daemons**

        # start all daemons
        bundle exec rake daemons:start

        # or start daemon one by one
        bundle exec rake daemon:matching:start
        ...

        # Daemon trade_executor can be run concurrently, e.g. below
        # line will start four trade executors, each with its own logfile.
        # Default to 1.
        TRADE_EXECUTOR=4 rake daemon:trade_executor:start

        # You can do the same when you start all daemons:
        TRADE_EXECUTOR=4 rake daemons:start

    When daemons don't work, check `log/#{daemon name}.rb.output` or `log/peatio:amqp:#{daemon name}.output` for more information (suffix is '.output', not '.log').

### IF USING MINA

    local~
    mina setup

    server~
    edit /home/deploy/bitnusa/shared/config/application.yml
    edit /home/deploy/bitnusa/shared/config/database.yml

    local~
    mina deploy (with line invoke :'rails:db_migrate' commented)
    
    server~
    bundle exec rake db:setup

    local~
    mina passenger:restart
    mina daemons:start

    UPDATE DEPLOYMENT:
    server~
    edit /home/deploy/bitnusa/shared/config/application.yml (if any)
    
    local~
    uncomment invoke :'migration:touch_accounts' with correct currency_id (if any new coins added)

    mina deploy
    mina daemons:stop
    mina daemons:start

### END IF

**SSL Certificate setting**

For security reason, you must setup SSL Certificate for production environment, if your SSL Certificated is been configured, please change the following line at `config/environments/production.rb`

    config.force_ssl = true

**Passenger:**

    sudo rm /etc/nginx/sites-enabled/default
    sudo ln -s /home/deploy/bitnusa/current/config/nginx.conf /etc/nginx/conf.d/peatio.conf
    sudo service nginx restart

**Liability Proof**

    # Add this rake task to your crontab so it runs regularly
    RAILS_ENV=production rake solvency:liability_proof

