<a name="civilized"></a>

## [Forum ini merupakan forum yang memiliki tata aturan dan terbuka untuk umum.](#civilized)

Dimohon untuk memperlakukan forum diskusi ini dengan rasa hormat yang selayaknya jika Anda berada dalam tempat umum. Kita juga merupakan tempat orang berkumpul untuk berbagi keterampilan, pengetahuan, dan minat melalui percakapan.

Aturan dibawah ini merupakan aturan yang tidak keras, hanyalah pedoman untuk membantu Anda dalam berdiskusi dan juga menjadikannya tempat yang nyaman untuk membuka wacana publik yang mengikuti tata aturan pada umumnya.

<a name="improve"></a>

## [Bantu Kami Untuk Meningkatkan Layanan dan Diskusi](#improve)

Bantu kami menjadikan tempat ini menjadi tempat yang tepat untuk diskusi dengan selalu berupaya dengan memberikan umpan balik kepada kami walaupun sekecil apapun itu. Jika Anda tidak yakin dalam memberikan pendapat, pikirkan kembali apa yang ingin Anda katakan dan coba lagi nanti.

Topik yang dibahas disini penting bagi kami, dan kami ingin Anda bertindak seolah-olah itu juga penting bagi Anda. Bersikaplah hormat kepada terhadap topik dan orang-orang yang mendiskusikannya, bahkan jika Anda tidak menyetujuinya dengan apa yang sedang dibahas atau dilontarkan.

Salah satu cara untuk memperbaiki adalah dengan menemukan yang sudah ada. Luangkan waktu untuk melihat topik disini sebelum membalas atau memulai opini Anda sendiri, dan Anda memiliki kesempatan yang lebih baik untuk bertemu dengan orang lain yang memiliki minat yang sama.

<a name="agreeable"></a>

## [Bersikaplah Baik, Bahkan Saat Anda Tidak Setuju](#agreeable)

Anda mungkin ingin menanggapi sesuatu dengan tidak sependapat dengannya. Tidak apa-apa. Tapi ingat untuk mengkritik ide, bukan orang. Tolong hindari:

*   Pemanggilan nama secara kasar dan tidak sopan
*   Menyerang pada satu pihak atau beberapa pihak dengan tidak sopan
*   Menanggapi konten dengan bukan maksud sebenarnya
*   Kontradiksi yang menyerang

Sebagai gantinya, berikan argumen kontra beralasan yang dapat memperbaiki percakapan.


<a name="participate"></a>

## [Partisipasi Anda Sangat Diharapkan](#participate)


Percakapan yang kita miliki di sini mengatur nada untuk setiap kedatangan baru. Bantu kami mempengaruhi masa depan komunitas ini dengan memilih untuk terlibat dalam diskusi yang menjadikan forum ini sebagai tempat yang menarik - dan hindari yang tidak pasti.

 

Discourse menyediakan alat yang memungkinkan para pengunjung forum untuk secara kolektif mengidentifikasi permasalahan ataupun hal yang menarik seperti: bookmarks (tandai), suka, flags (memberikan bendera), balasan, suntingan, dan lain halnya. Gunakan alat ini untuk Anda sendiri dalam berselancar di forum ini.

Mari kita tinggalkan komunitas kita lebih baik dibandingkan dengan sebelumnya.

<a name="flag-problems"></a>

## [Jika Anda melihat masalah, tandai!](#flag-problems)

Moderator memiliki wewenang khusus, mereka akan bertanggungjawab untuk forum ini. Tapi begitu juga Anda. Dengan bantuan Anda, moderator bisa menjadi fasilitator yang baik.

Bila Anda melihat perilaku yang buruk, jangan dibalas. Ini mendorong pelaku perilaku buruk untuk mengulangi perbuatan yang sama. Juga itu akan mengonsumsi energi Anda dan waktu orang lain. Cukup memberikan peringatan atau memberikan bendera, moderator akan menindaklanjuti

 
Untuk menjaga komunitas kami, moderator berhak menghapus konten dan akun pengguna apapun dengan alasan apapun kapan saja. Moderator tidak mempratinjau posting baru; para moderator dan operator situs tidak bertanggung jawab atas konten yang dibagikan oleh pengunjung forum.


<a name="be-civil"></a>

## [Selalu Menjadi Pengunjung Forum yang Baik](#be-civil)

Tidak ada untungnya untungnya menyabotase percakapan 
*   Jadilah pengunjung forum yang baik jangan memberikan sesuatu yang bersifat menyinggung, kasar, atau penebar kebencian.
*   Jagalah forum agar tetap menjadi tempat yang nyaman untuk semuanya.
*   Hormati forum kami seperti Anda menghormati orang lain di tempat umum dengan tidak 
membagikan sesuatu yang berbau SARA.

Forum ini adalah forum publik dan Anda dapat mencari apa yang Anda katakan didalam kotak pencarian. Jaga forum ini tetap bersih dan aman untuk semuanya.

<a name="keep-tidy"></a>

## [Tetaplah Sesuai Aturan](#keep-tidy)

Forum ini adalah forum publik dan Anda dapat mencari apa yang Anda katakan didalam kotak pencarian. Jaga forum ini tetap bersih dan aman untuk semuanya.

*   Berusahalah untuk Mengikuti Tema dan juga Sub-tema Forum.
*   Jangan memulai topik dalam kategori yang salah
*   Jangan cross-post hal yang sama dalam beberapa topik.
*   Jangan mengirimkan balasan tanpa isi.
*  Jangan menandatangani posting Anda - setiap posting memiliki informasi profil Anda yang menyertainya.
Daripada mengeposkan "+1" atau "Setuju", gunakan tombol Suka. Alih-alih mengambil topik yang ada dalam arah yang berbeda secara radikal, gunakan Balas sebagai Topik yang Terhubung.

<a name="stealing"></a>

## [Unggah yang memang milik kamu sendiri](#stealing)

Anda tidak seharusnya mengunggah hal-hal bersifat digital yang bukan milik Anda tanpa persetujuan apapun. Anda tidak seharusnya mengunggah tulisan, deskripsi, gambar, tautan, atau media lainnya yang mengandung hak intelitektual pihak yang lain (Perangkat lunak, gambar, cuplikan suara, dan semacamnya)

<a name="power"></a>

## [Situs ini dioperasikan oleh staff lokal yang ramah dan Anda sendiri](#power)
Jika Anda memiliki pertanyaan tentang bagaimana kami mengelola forum ini, Anda dapat memberikan pertanyaan atau membuka topik baru di forum ini. Jika ada hal yang mendesak Anda dapat menghubungi kontak kami di homepage kami
<a name="tos"></a>

## [Ketentuan layanan](#tos)

Kami memiliki Persyaratan Layanan yang menjelaskan perilaku dan hak Anda yang terkait dengan isi, privasi , dan undang-undang. Untuk menggunakan layani ini Anda harus menyetujui Terms of Service berikut [TOS](/tos).