require 'mina/bundler'
require 'mina/rails'
require 'mina/git'
require 'mina/rbenv'
require 'mina/slack/tasks'

set :repository, 'git@gitlab.com:irwangunardi/bitnusa.git'
set :user, 'deploy'
set :deploy_to, '/home/deploy/bitnusa'
set :branch, 'master'
set :term_mode, nil
set :domain, 'www.bitnusa.com'
set :rails_env, 'production'

set :shared_paths, [
  'config/database.yml',
  'config/application.yml',
  'public/uploads',
  'tmp',
  'log'
]

task :staging do
  set :domain, 'beta.bitnusa.com'
  set :rails_env, 'staging'
end

task :environment do
  invoke :'rbenv:load'
end

task :setup => :environment do
  queue! %[mkdir -p "#{deploy_to}/shared/log"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/log"]

  queue! %[mkdir -p "#{deploy_to}/shared/config"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/config"]

  queue! %[mkdir -p "#{deploy_to}/shared/tmp"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/tmp"]

  queue! %[mkdir -p "#{deploy_to}/shared/public/uploads"]
  queue! %[chmod g+rx,u+rwx "#{deploy_to}/shared/public/uploads"]

  queue! %[touch "#{deploy_to}/shared/config/database.yml"]
  queue! %[touch "#{deploy_to}/shared/config/application.yml"]
end

desc "Deploys the current version to the server."
task deploy: :environment do
  deploy do
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :'rails:db_migrate' #Comment this on the first run, then ssh and run db:setup manually
    #invoke :'migration:touch_accounts'#Uncomment this only there is new coin added, edit according currency_id
    invoke :'rails:touch_client_i18n_assets'
    invoke :'rails:assets_precompile'

    to :launch do
      invoke :'passenger:restart'
    end
  end
end

namespace :passenger do
  desc "Restart Passenger"
  task :restart do
    queue %{
      echo "-----> Restarting passenger"
      cd #{deploy_to}/current
      #{echo_cmd %[mkdir -p tmp]}
      #{echo_cmd %[touch tmp/restart.txt]}
    }
  end
end

namespace :migration do
  desc "Touch New Account"
  task touch_accounts: :environment do
    queue %{
      cd #{deploy_to}/current
      RAILS_ENV=#{rails_env} bundle exec ./bin/rake migration:touch_accounts currency_id=0
      echo Touch Accounts START DONE!!!
    }
  end
end

namespace :rails do
  task :touch_client_i18n_assets do
    queue %[
      echo "-----> Touching clint i18n assets"
      #{echo_cmd %[RAILS_ENV=#{rails_env} bundle exec rake deploy:touch_client_i18n_assets]}
    ]
  end
end

namespace :daemons do
  desc "Start Daemons"
  task start: :environment do
    queue %{
      cd #{deploy_to}/current
      RAILS_ENV=#{rails_env} bundle exec ./bin/rake daemons:start
      echo Daemons START DONE!!!
    }
  end

  desc "Stop Daemons"
  task stop: :environment do
    queue %{
      cd #{deploy_to}/current
      RAILS_ENV=#{rails_env} bundle exec ./bin/rake daemons:stop
      echo Daemons STOP DONE!!!
    }
  end

  desc "Restart Daemons"
  task restart: :environment do
    queue %{
      cd #{deploy_to}/current
      RAILS_ENV=#{rails_env} bundle exec ./bin/rake daemons:stop
      echo Daemons STOP DONE!!!
      RAILS_ENV=#{rails_env} bundle exec ./bin/rake daemons:start
      echo Daemons START DONE!!!
    }
  end

  desc "Query Daemons"
  task status: :environment do
    queue %{
      cd #{deploy_to}/current
      RAILS_ENV=#{rails_env} bundle exec ./bin/rake daemons:status
    }
  end
end

desc "Generate liability proof"
task 'solvency:liability_proof' do
  queue "cd #{deploy_to}/current && RAILS_ENV=#{rails_env} bundle exec rake solvency:liability_proof"
end
