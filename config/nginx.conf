server {
  listen 80;
  return 301 https://$host$request_uri;
}

server {
  listen 443 ssl http2 default;
  server_name _;
  passenger_enabled on;
  # passenger_app_env "staging";
  gzip on;

  root /home/deploy/bitnusa/current/public;

  ssl                  on;
  ssl_certificate      /etc/nginx/ssl/bitnusa_com/ssl-bundle.crt;
  ssl_certificate_key  /etc/nginx/ssl/bitnusa_com/bitnusa.key;

  ssl_ciphers ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS;
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
  ssl_prefer_server_ciphers on;
  ssl_session_cache shared:SSL:10m;
  ssl_session_timeout  5m;
  add_header Strict-Transport-Security "max-age=63072000; includeSubDomains";
  add_header X-Frame-Options DENY;
  add_header X-Content-Type-Options nosniff;

  # ssl_stapling on; # Requires nginx >= 1.3.7
  # ssl_stapling_verify on; # Requires nginx => 1.3.7
  # resolver 8.8.4.4 8.8.8.8 valid=300s;
  # resolver_timeout 5s;

  location = /favicon.png {
    expires    max;
    add_header Cache-Control public;
  }

  location = /ZeroClipboard.swf {
    expires    max;
    add_header Cache-Control public;
  }

  location ~ ^/(assets)/  {
    gzip_static on;
    expires     max;
    add_header  Cache-Control public;
  }

  # disable gzip on all omniauth paths to prevent BREACH
  location ~ ^/auth/ {
    gzip off;
    passenger_enabled on;
  }
}
