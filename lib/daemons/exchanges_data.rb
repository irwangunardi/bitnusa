#!/usr/bin/env ruby

ENV["RAILS_ENV"] ||= "development"

root = File.expand_path(File.dirname(__FILE__))
root = File.dirname(root) until File.exists?(File.join(root, 'config'))
Dir.chdir(root)

require File.join(root, "config", "environment")

$running = true
Signal.trap("TERM") do
  $running = false
end

while($running) do
  all_tickers = {}
  Market.all.each do |market|
    global = Global[market.id]
    market.fetch_and_cache_exchanges_data
    global.trigger_exchanges_data
    sleep 1
  end

  sleep 60
end
