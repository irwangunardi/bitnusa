#!/usr/bin/env ruby

# You might want to change this
ENV["RAILS_ENV"] ||= "development"

root = File.expand_path(File.dirname(__FILE__))
root = File.dirname(root) until File.exists?(File.join(root, 'config'))
Dir.chdir(root)

require File.join(root, "config", "environment")

$running = true
Signal.trap("TERM") do
  $running = false
end

while($running) do
  Currency.all.each do |currency|
    begin
      currency.refresh_balance
      if (currency.wallet_maintainance?)
        currency.remove_temporary_wallet_maintainance
      end
    rescue
      currency.set_temporary_wallet_maintainance
    end
  end

  sleep 15
end
