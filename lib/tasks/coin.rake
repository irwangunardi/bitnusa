namespace :coin do
  desc "Sync coin deposit transactions"
  task sync_deposit: :environment do
    #example: coin:sync_deposit code=btc number=50 reprocess=1 
    #example: coin:sync_deposit manualtx=0x8329ab787cba7bca
    code    = ENV['code']
    number  = ENV['number'] ? ENV['number'].to_i : 100
    channel = DepositChannel.find_by_currency(code)
    manualtx = ENV['manualtx']

    if channel.blank?
      puts "Can not find the deposit channel by code: #{code}"
      exit 0
    end

    if (manualtx)
      puts "Processing #{manualtx} missed transaction."
      AMQPQueue.enqueue :deposit_coin, { txid: manualtx, channel_key: channel.key }
    else
      missed = []
      transactions = {}

      if code == 'eth' || code == 'etc'
        transactions = CoinRPC[code].safe_listtransactions(number)
      else
        transactions = CoinRPC[code].safe_listtransactions('payment', number).select { |tx| tx['category'] == 'receive' }
      end

      transactions.each do |tx|
        unless PaymentTransaction::Normal.where(txid: tx['txid'], address: tx['address']).first
          puts "Missed txid:#{tx['txid']} address:#{tx['address']} (#{tx['amount']})"
          missed << tx
        end
      end
      puts "#{missed.size} missed transactions found."

      if ENV['reprocess'] == '1' && missed.size > 0
        puts "Reprocessing .."
        missed.each do |tx|
          AMQPQueue.enqueue :deposit_coin, { txid: tx['txid'], channel_key: channel.key }
        end
        puts "Done."
      end

    end
  end
end
